# Install test requirements for separate local testing

# This is basically not needed when testing fro gitlab CI, because Tryton dependencies
# are built from the mono repos. But for local testing tox pulls from Pypi and we need
# to checkout unreleased packages from git.

# tryton-mono
git+https://gitlab.com/mbsolutions/tryton-mono.git@mbs-6.0#subdirectory=modules/company
