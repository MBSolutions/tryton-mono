#!/usr/bin/env python3
# -*- coding: utf-8 -*-
#
# Generate the currency_symbols file
# uses forex_python not available in Debian
#
# Copyright (C) 2021 Mathias Behrle <mbehrle@m9s.biz>
# Copyright (C) 2021 MBSolutions
# The COPYRIGHT file at the top level of this repository
# contains the full license terms.
import pycountry
from forex_python.converter import CurrencyCodes

codes = CurrencyCodes()

with open('currency_symbols.py', 'w') as c:
    c.write('#!/usr/bin/env python3\n')
    c.write('# -*- coding: utf-8 -*-\n')
    c.write('# The COPYRIGHT file at the top level of this repository\n')
    c.write('# contains the full license terms.\n')
    c.write('\n')
    c.write('symbols = {\n')
    for currency in pycountry.currencies:
        symbol = CurrencyCodes().get_symbol(currency.alpha_3)
        if symbol:
            c.write('%s%s%s%s%s\n' %
                ("    '", currency.alpha_3, "': '", symbol, "',"))
    c.write('}\n')
