#
msgid ""
msgstr "Content-Type: text/plain; charset=utf-8\n"

msgctxt "field:account.payment,stripe_account:"
msgid "Stripe Account"
msgstr "Stripe Konto"

msgctxt "field:account.payment,stripe_amount:"
msgid "Stripe Amount"
msgstr "Stripe Betrag"

msgctxt "field:account.payment,stripe_capturable:"
msgid "Stripe Capturable"
msgstr "Stripe Abrechenbar"

msgctxt "field:account.payment,stripe_capture:"
msgid "Stripe Capture"
msgstr "Stripe Abrechnung"

msgctxt "field:account.payment,stripe_capture_needed:"
msgid "Stripe Capture Needed"
msgstr "Stripe Abrechnung notwendig"

msgctxt "field:account.payment,stripe_captured:"
msgid "Stripe Captured"
msgstr "Stripe Abgerechnet"

msgctxt "field:account.payment,stripe_charge_id:"
msgid "Stripe Charge ID"
msgstr "Stripe Charge ID"

msgctxt "field:account.payment,stripe_chargeable:"
msgid "Stripe Chargeable"
msgstr "Stripe Zahlbar"

msgctxt "field:account.payment,stripe_checkout_id:"
msgid "Stripe Checkout ID"
msgstr "Stripe Checkout ID"

msgctxt "field:account.payment,stripe_checkout_needed:"
msgid "Stripe Checkout Needed"
msgstr "Stripe Checkout erforderlich"

msgctxt "field:account.payment,stripe_customer:"
msgid "Stripe Customer"
msgstr "Stripe Kunde"

msgctxt "field:account.payment,stripe_customer_payment_method:"
msgid "Stripe Payment Method"
msgstr "Stripe Zahlungsmethode"

msgctxt "field:account.payment,stripe_customer_payment_method_selection:"
msgid "Stripe Customer Payment Method"
msgstr "Stripe Kunde Zahlungsmethode"

msgctxt "field:account.payment,stripe_customer_source:"
msgid "Stripe Customer Source"
msgstr "Stripe Kunde Ursprung"

msgctxt "field:account.payment,stripe_customer_source_selection:"
msgid "Stripe Customer Source"
msgstr "Stripe Kunde Ursprung"

msgctxt "field:account.payment,stripe_dispute_reason:"
msgid "Stripe Dispute Reason"
msgstr "Stripe Rückbelastungsgrund"

msgctxt "field:account.payment,stripe_dispute_status:"
msgid "Stripe Dispute Status"
msgstr "Stripe Rückbelastungsstatus"

msgctxt "field:account.payment,stripe_error_code:"
msgid "Stripe Error Code"
msgstr "Stripe Fehlercode"

msgctxt "field:account.payment,stripe_error_message:"
msgid "Stripe Error Message"
msgstr "Stripe Fehlermeldung"

msgctxt "field:account.payment,stripe_error_param:"
msgid "Stripe Error Param"
msgstr "Stripe Fehlerparameter"

msgctxt "field:account.payment,stripe_idempotency_key:"
msgid "Stripe Idempotency Key"
msgstr "Stripe Idempotenz-Schlüssel"

msgctxt "field:account.payment,stripe_payment_intent_id:"
msgid "Stripe Payment Intent"
msgstr "Stripe Zahlungsabsicht"

msgctxt "field:account.payment,stripe_refunds:"
msgid "Refunds"
msgstr "Erstattungen"

msgctxt "field:account.payment,stripe_token:"
msgid "Stripe Token"
msgstr "Stripe Token"

msgctxt "field:account.payment.journal,stripe_account:"
msgid "Account"
msgstr "Konto"

msgctxt "field:account.payment.stripe.account,last_event:"
msgid "Last Event"
msgstr "Letztes Ereignis"

msgctxt "field:account.payment.stripe.account,name:"
msgid "Name"
msgstr "Name"

msgctxt "field:account.payment.stripe.account,publishable_key:"
msgid "Publishable Key"
msgstr "Publizierbarer Schlüssel"

msgctxt "field:account.payment.stripe.account,secret_key:"
msgid "Secret Key"
msgstr "Geheimer Schlüssel"

msgctxt "field:account.payment.stripe.account,webhook_endpoint:"
msgid "Webhook Endpoint"
msgstr "Webhook Endpunkt"

msgctxt "field:account.payment.stripe.account,webhook_identifier:"
msgid "Webhook Identifier"
msgstr "Webhook Identifikator"

msgctxt "field:account.payment.stripe.account,webhook_signing_secret:"
msgid "Webhook Signing Secret"
msgstr "Webhook Geheimer Signaturschlüssel"

msgctxt "field:account.payment.stripe.customer,party:"
msgid "Party"
msgstr "Partei"

msgctxt "field:account.payment.stripe.customer,stripe_account:"
msgid "Account"
msgstr "Konto"

msgctxt "field:account.payment.stripe.customer,stripe_checkout_id:"
msgid "Stripe Checkout ID"
msgstr "Stripe Checkout ID"

msgctxt "field:account.payment.stripe.customer,stripe_checkout_needed:"
msgid "Stripe Checkout Needed"
msgstr "Stripe Checkout erforderlich"

msgctxt "field:account.payment.stripe.customer,stripe_customer_id:"
msgid "Stripe Customer ID"
msgstr "Stripe Kunde ID"

msgctxt "field:account.payment.stripe.customer,stripe_error_code:"
msgid "Stripe Error Code"
msgstr "Stripe Fehlercode"

msgctxt "field:account.payment.stripe.customer,stripe_error_message:"
msgid "Stripe Error Message"
msgstr "Stripe Fehlermeldung"

msgctxt "field:account.payment.stripe.customer,stripe_error_param:"
msgid "Stripe Error Param"
msgstr "Stripe Fehlerparameter"

msgctxt "field:account.payment.stripe.customer,stripe_setup_intent_id:"
msgid "Stripe SetupIntent ID"
msgstr "Stripe Setupintent ID"

msgctxt "field:account.payment.stripe.customer,stripe_token:"
msgid "Stripe Token"
msgstr "Stripe Token"

msgctxt "field:account.payment.stripe.customer.source.detach.ask,customer:"
msgid "Customer"
msgstr "Kunde"

msgctxt "field:account.payment.stripe.customer.source.detach.ask,source:"
msgid "Source"
msgstr "Source"

msgctxt "field:account.payment.stripe.refund,amount:"
msgid "Amount"
msgstr "Betrag"

msgctxt "field:account.payment.stripe.refund,currency_digits:"
msgid "Currency Digits"
msgstr "Nachkommastellen Währung"

msgctxt "field:account.payment.stripe.refund,payment:"
msgid "Payment"
msgstr "Zahlung"

msgctxt "field:account.payment.stripe.refund,reason:"
msgid "Reason"
msgstr "Grund"

msgctxt "field:account.payment.stripe.refund,state:"
msgid "State"
msgstr "Status"

msgctxt "field:account.payment.stripe.refund,stripe_amount:"
msgid "Stripe Amount"
msgstr "Stripe Betrag"

msgctxt "field:account.payment.stripe.refund,stripe_error_code:"
msgid "Stripe Error Code"
msgstr "Stripe Fehlercode"

msgctxt "field:account.payment.stripe.refund,stripe_error_message:"
msgid "Stripe Error Message"
msgstr "Stripe Fehlermeldung"

msgctxt "field:account.payment.stripe.refund,stripe_error_param:"
msgid "Stripe Error Param"
msgstr "Stripe Fehlerparameter"

msgctxt "field:account.payment.stripe.refund,stripe_idempotency_key:"
msgid "Stripe Idempotency Key"
msgstr "Stripe Idempotenz-Schlüssel"

msgctxt "field:account.payment.stripe.refund,stripe_refund_id:"
msgid "Stripe Refund ID"
msgstr "Stripe Refund ID"

msgctxt "field:party.party,stripe_customers:"
msgid "Stripe Customers"
msgstr "Stripe Kunden"

msgctxt "help:account.payment.stripe.account,webhook_endpoint:"
msgid "The URL to be called by Stripe."
msgstr "Die von Stripe aufzurufende URL."

msgctxt "help:account.payment.stripe.account,webhook_signing_secret:"
msgid "The Stripe's signing secret of the webhook."
msgstr "Der geheime Signaturschlüssel von Stripe für den Webhook."

msgctxt "model:account.payment.stripe.account,name:"
msgid "Stripe Account"
msgstr "Stripe Konto"

msgctxt "model:account.payment.stripe.customer,name:"
msgid "Stripe Customer"
msgstr "Stripe Kunde"

msgctxt "model:account.payment.stripe.customer.source.detach.ask,name:"
msgid "Detach Customer Source"
msgstr "Kunde Ursprung Lösen"

msgctxt "model:account.payment.stripe.refund,name:"
msgid "Stripe Payment Refund"
msgstr "Stripe Zahlung Erstattung"

msgctxt "model:ir.action,name:act_account_form"
msgid "Stripe Accounts"
msgstr "Stripe Konten"

msgctxt "model:ir.action,name:act_customer_form"
msgid "Stripe Customers"
msgstr "Stripe Kunden"

msgctxt "model:ir.action,name:act_refund_form"
msgid "Stripe Refunds"
msgstr "Stripe Erstattungen"

msgctxt "model:ir.action,name:report_checkout"
msgid "Stripe Checkout"
msgstr "Stripe Checkout"

msgctxt "model:ir.action,name:report_email_checkout"
msgid "Checkout"
msgstr "Checkout"

msgctxt "model:ir.action,name:url_checkout"
msgid "Stripe Checkout"
msgstr "Stripe Checkout"

msgctxt "model:ir.action,name:wizard_checkout"
msgid "Stripe Checkout"
msgstr "Stripe Checkout"

msgctxt "model:ir.action,name:wizard_customer_source_detach"
msgid "Detach Source"
msgstr "Ursprung lösen"

msgctxt "model:ir.action.act_window.domain,name:act_refund_form_domain_all"
msgid "All"
msgstr "Alle"

msgctxt ""
"model:ir.action.act_window.domain,name:act_refund_form_domain_approved"
msgid "Approved"
msgstr "Genehmigt"

msgctxt "model:ir.action.act_window.domain,name:act_refund_form_domain_draft"
msgid "Draft"
msgstr "Entwurf"

msgctxt "model:ir.action.act_window.domain,name:act_refund_form_domain_failed"
msgid "Failed"
msgstr "Fehlgeschlagen"

msgctxt ""
"model:ir.action.act_window.domain,name:act_refund_form_domain_processing"
msgid "Processing"
msgstr "In Ausführung"

msgctxt ""
"model:ir.action.act_window.domain,name:act_refund_form_domain_suceeded"
msgid "Succeeded"
msgstr "Erfolgreich"

msgctxt "model:ir.message,text:msg_no_stripe_token"
msgid ""
"To process payment \"%(payment)s\" you must set a Stripe token or customer."
msgstr ""
"Um Zahlung \"%(payment)s\" ausführen zu können, muss ein Stripe Token oder "
"ein Kunde erfasst werden."

msgctxt "model:ir.message,text:msg_stripe_receivable"
msgid "To pay \"%(payment)s\", you cannot use a stripe journal \"%(journal)s\"."
msgstr ""
"\"(%payment)s\" kann nicht über das Stripe Journal \"%(journal)s\" bezahlt "
"werden."

msgctxt "model:ir.model.button,confirm:account_new_identifier_button"
msgid ""
"This action will make the previous URL unusable. Do you want to continue?"
msgstr ""
"Diese Aktion macht die vorherige URL unbrauchbar. Möchten Sie fortsetzen?"

msgctxt "model:ir.model.button,string:account_new_identifier_button"
msgid "New URL"
msgstr "Neue URL erzeugen"

msgctxt "model:ir.model.button,string:customer_source_detach_button"
msgid "Detach Source"
msgstr "Ursprung lösen"

msgctxt "model:ir.model.button,string:customer_stripe_checkout_button"
msgid "Add Card"
msgstr "Karte hinzufügen"

msgctxt "model:ir.model.button,string:payment_stripe_capture_button"
msgid "Stripe Capture"
msgstr "Stripe Abrechnen"

msgctxt "model:ir.model.button,string:payment_stripe_checkout_button"
msgid "Stripe Checkout"
msgstr "Stripe Checkout"

msgctxt "model:ir.model.button,string:refund_approve_button"
msgid "Approve"
msgstr "Genehmigen"

msgctxt "model:ir.model.button,string:refund_draft_button"
msgid "Draft"
msgstr "Entwurf"

msgctxt "model:ir.ui.menu,name:menu_account_form"
msgid "Stripe Accounts"
msgstr "Stripe Konten"

msgctxt "model:ir.ui.menu,name:menu_customer_form"
msgid "Stripe Customers"
msgstr "Stripe Kunden"

msgctxt "model:ir.ui.menu,name:menu_refund_form"
msgid "Stripe Refunds"
msgstr "Stripe Erstattungen"

msgctxt "report:account.payment.stripe.checkout:"
msgid "Credit or debit card"
msgstr "Kredit- oder Debitkarte"

msgctxt "report:account.payment.stripe.checkout:"
msgid "Pay"
msgstr "Bezahlen"

msgctxt "report:account.payment.stripe.checkout:"
msgid "Register card"
msgstr "Karte registrieren"

msgctxt "report:account.payment.stripe.checkout:"
msgid "Stripe Checkout"
msgstr "Stripe Checkout"

msgctxt "report:account.payment.stripe.email_checkout:"
msgid "Authorization needed"
msgstr "Autorisierung erforderlich"

msgctxt "report:account.payment.stripe.email_checkout:"
msgid "Button is not working? Paste this into your browser:"
msgstr ""
"Wenn der Button nicht funktioniert, kopieren Sie bitte die folgende Zeile in"
" die Adresszeile Ihres Browsers:"

msgctxt "report:account.payment.stripe.email_checkout:"
msgid "Checkout"
msgstr "Checkout"

msgctxt "report:account.payment.stripe.email_checkout:"
msgid "If you didn't make this request, you can ignore this email."
msgstr ""
"Ignorieren Sie bitte diese E-Mail, wenn Sie nicht der Urheber dieser Anfrage"
" sind."

msgctxt "report:account.payment.stripe.email_checkout:"
msgid "You need to authorize the payment"
msgstr "Die Zahlung muss autorisiert werden"

msgctxt "selection:account.payment.journal,process_method:"
msgid "Stripe"
msgstr "Stripe"

msgctxt "selection:account.payment.stripe.refund,reason:"
msgid "Duplicate"
msgstr "Duplikat"

msgctxt "selection:account.payment.stripe.refund,reason:"
msgid "Fraudulent"
msgstr "Betrügerisch"

msgctxt "selection:account.payment.stripe.refund,reason:"
msgid "Requested by Customer"
msgstr "Vom Kunden angefordert"

msgctxt "selection:account.payment.stripe.refund,state:"
msgid "Approved"
msgstr "Genehmigt"

msgctxt "selection:account.payment.stripe.refund,state:"
msgid "Draft"
msgstr "Entwurf"

msgctxt "selection:account.payment.stripe.refund,state:"
msgid "Failed"
msgstr "Fehlgeschlagen"

msgctxt "selection:account.payment.stripe.refund,state:"
msgid "Processing"
msgstr "In Ausführung"

msgctxt "selection:account.payment.stripe.refund,state:"
msgid "Succeeded"
msgstr "Erfolgreich"

msgctxt "selection:ir.cron,method:"
msgid "Capture Stripe Payments"
msgstr "Stripe Zahlungen abrechnen"

msgctxt "selection:ir.cron,method:"
msgid "Charge Stripe Payments"
msgstr "Stripe Zahlungen reservieren"

msgctxt "selection:ir.cron,method:"
msgid "Create Stripe Customer"
msgstr "Stripe Kunden erstellen"

msgctxt "selection:ir.cron,method:"
msgid "Create Stripe Refund"
msgstr "Stripe Erstattung erstellen"

msgctxt "selection:ir.cron,method:"
msgid "Delete Stripe Customer"
msgstr "Stripe Kunden löschen"

msgctxt "selection:ir.cron,method:"
msgid "Fetch Stripe Events"
msgstr "Stripe Ereignisse abrufen"

msgctxt "selection:ir.cron,method:"
msgid "Update Stripe Intent Customer"
msgstr "Stripe Intent Customer aktualisieren"

msgctxt "view:account.payment.journal:"
msgid "Stripe"
msgstr "Stripe"

msgctxt "view:account.payment:"
msgid "Capturable:"
msgstr "Abrechenbar:"

msgctxt "view:account.payment:"
msgid "Capture:"
msgstr "Abrechnung:"

msgctxt "view:account.payment:"
msgid "Charge ID:"
msgstr "Charge ID:"

msgctxt "view:account.payment:"
msgid "Chargeable:"
msgstr "Abrechenbar:"

msgctxt "view:account.payment:"
msgid "Customer:"
msgstr "Kunde:"

msgctxt "view:account.payment:"
msgid "Payment Intent:"
msgstr "Zahlungsabsicht:"

msgctxt "view:account.payment:"
msgid "Payment Method:"
msgstr "Zahlungsmethode:"

msgctxt "view:account.payment:"
msgid "Source:"
msgstr "Source:"

msgctxt "view:account.payment:"
msgid "Stripe"
msgstr "Stripe"

msgctxt "view:account.payment:"
msgid "Token:"
msgstr "Token:"

msgctxt ""
"wizard_button:account.payment.stripe.customer.source.detach,ask,detach:"
msgid "Detach"
msgstr "Abtrennen"

msgctxt "wizard_button:account.payment.stripe.customer.source.detach,ask,end:"
msgid "Cancel"
msgstr "Abbrechen"
