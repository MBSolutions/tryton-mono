:orphan:

.. _index-analytic-accounting:

Analytic Accounting
===================

`Account </projects/modules-analytic-account/en/6.0>`_
    Fundamentals for analytic.

`Invoice </projects/modules-analytic-invoice/en/6.0>`_
    Adds analytic on invoice.

`Purchase </projects/modules-analytic-purchase/en/6.0>`_
    Adds analytic on purchase.

`Sale </projects/modules-analytic-sale/en/6.0>`_
    Adds analytic on sale.
