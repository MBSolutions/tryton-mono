:orphan:

.. _index-purchase:

Purchase
========

`Purchase </projects/modules-purchase/en/6.0>`_
    Fundamentals to follow purchases.

`Amendment </projects/modules-purchase-amendment/en/6.0>`_
    Amend purchases in processing.

`History </projects/modules-purchase-history/en/6.0>`_
    Manages revisions.

`Invoice Line Standalone </projects/modules-purchase-invoice-line-standalone/en/6.0>`_
    Generates invoice lines.

`Price List </projects/modules-purchase-price-list/en/6.0>`_
    Applies price list.

`Request </projects/modules-purchase-request/en/6.0>`_
    Collects the purchase requests.

`Request Quotation </projects/modules-purchase-request-quotation/en/6.0>`_
    Asks quotation for the purchase requests.

`Requisition </projects/modules-purchase-requisition/en/6.0>`_
    Requires purchase by employees.

`Secondary Unit </projects/modules-purchase-secondary-unit/en/6.0>`_
    Adds a secondary unit of measure.

`Shipment Cost </projects/modules-purchase-shipment-cost/en/6.0>`_
    Computes shipment cost.
