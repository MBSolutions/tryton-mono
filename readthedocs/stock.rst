:orphan:

.. _index-stock:

Stock
=====

`Stock </projects/modules-stock/en/6.0>`_
    Fundamentals to manage warehouses.

`Assign Manual </projects/modules-stock-assign-manual/en/6.0>`_
    Manually decide where to pick products.

`Consignment </projects/modules-stock-consignment/en/6.0>`_
    Manages consignment stock from supplier or at customer warehouse.

`Forecast </projects/modules-stock-forecast/en/6.0>`_
    Forecast the stock levels.

`Inventory Location </projects/modules-stock-inventory-location/en/6.0>`_
    Creates inventories for many locations.

`Location Move </projects/modules-stock-location-move/en/6.0>`_
    Makes location movable.

`Location Sequence </projects/modules-stock-location-sequence/en/6.0>`_
    Orders locations.

`Lot </projects/modules-stock-lot/en/6.0>`_
    Track products with lot.

`Lot Shelf Life Expiration Date </projects/modules-stock-lot-sled/en/6.0>`_
    Manages expiration dates.

`Lot Unit </projects/modules-stock-lot-unit/en/6.0>`_
    Defines unit and quantity per lot.

`Package </projects/modules-stock-package/en/6.0>`_
    Store packaging on shipments.

`Package Shipping </projects/modules-stock-package-shipping/en/6.0>`_
    Fundamentals to interact with shipping services.

`Package Shipping DPD </projects/modules-stock-package-shipping-dpd/en/6.0>`_
    Connects with DPD shipping service.

`Package Shipping UPS </projects/modules-stock-package-shipping-ups/en/6.0>`_
    Connects with UPS shipping service.

`Product Location </projects/modules-stock-product-location/en/6.0>`_
    Defines preferred locations for products.

`Quantity Early Planning </projects/modules-stock-quantity-early-planning/en/6.0>`_
    Consume earlier stock.

`Quantity Issue </projects/modules-stock-quantity-issue/en/6.0>`_
    Reports quantity issues.

`Secondary Unit </projects/modules-stock-secondary-unit/en/6.0>`_
    Adds a secondary unit of measure.

`Shipment Cost </projects/modules-stock-shipment-cost/en/6.0>`_
    Adds shipment costs to outgoing moves.

`Shipment Measurements </projects/modules-stock-shipment-measurements/en/6.0>`_
    Adds measurements to shipments.

`Split </projects/modules-stock-split/en/6.0>`_
    Splits moves and shipments.

`Supply </projects/modules-stock-supply/en/6.0>`_
    Supplies warehouses.

`Supply Forecast </projects/modules-stock-supply-forecast/en/6.0>`_
    Uses forecast to supply warehouses.

`Supply Day </projects/modules-stock-supply-day/en/6.0>`_
    Compute supply date per week day.

`Supply Production </projects/modules-stock-supply-production/en/6.0>`_
    Supplies warehouses with production orders.
