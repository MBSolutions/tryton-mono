:orphan:

.. _index-edocument:

Electronic Document
===================

`UN/CEFACT </projects/modules-edocument-uncefact/en/6.0>`_
    Supports UN/CEFACT format.

`UNECE </projects/modules-edocument-unece/en/6.0>`_
    Adds UNECE codes.
