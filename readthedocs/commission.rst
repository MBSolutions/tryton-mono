:orphan:

.. _index-commission:

Commission
==========

`Commission </projects/modules-commission/en/6.0>`_
    Fundamentals to commission sale's agents.

`Waiting </projects/modules-commission-waiting/en/6.0>`_
    Creates waiting account moves.
